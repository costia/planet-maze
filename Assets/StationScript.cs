﻿using UnityEngine;
using System.Collections;

public class StationScript : MonoBehaviour {
	public PlayerScript mPlayer;
	public GUIText mText;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		float dist= (transform.position-mPlayer.transform.position).magnitude;
		if (dist<10){
			mText.text="WON";
			mText.color=Color.green;
		}

	
	}
}
