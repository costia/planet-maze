﻿using UnityEngine;
using System.Collections;

public class PlanetScript : MonoBehaviour {
	public Vector3 mAxis;
	public float mSpeed,mRotationRadius,mPhase;
	GameObject mSun;
	
	
	Vector3 mA,mB;
	// Use this for initialization
	void Start () {
		mA=new Vector3(-mAxis.y,mAxis.x,0);
		if (mA.magnitude==0){
			mA=new Vector3(0,mAxis.z,-mAxis.y);
		}
		mA.Normalize();
		mB=Vector3.Cross(mA,mAxis.normalized);
		mSun=GameObject.Find("Sun");

		float PI=Mathf.PI;
		float theta_scale = 0.1f;             //Set lower to add more points
		int size = (int)Mathf.Ceil((2.0f * PI) / theta_scale); //Total number of points in circle.
		
		LineRenderer lineRenderer = gameObject.AddComponent<LineRenderer>();
		lineRenderer.material = new Material(Shader.Find("Self-Illumin/Diffuse"));
		lineRenderer.SetColors(Color.red, Color.green);
		lineRenderer.SetWidth(0.2F, 0.2F);
		lineRenderer.SetVertexCount(size+1);
		
		int i = 0;
		for(float theta = 0; theta < 2 * PI+0.1f; theta += 0.1f) {
			Vector3 pos;

			pos=new Vector3(Mathf.Cos(theta)*mA.x+Mathf.Sin(theta)*mB.x,Mathf.Cos(theta)*mA.y+Mathf.Sin(theta)*mB.y,Mathf.Cos(theta)*mA.z+Mathf.Sin(theta)*mB.z);
			pos=pos*mRotationRadius+transform.parent.position;
			lineRenderer.SetPosition(i, pos);
			i+=1;
		}
		
	}
	
	public void Select(){
		GetComponent<Renderer>().material.color=Color.green;
	}
	
	public void DeSelect(){
		GetComponent<Renderer>().material.color=Color.white;
	}
	// Update is called once per frame
	void Update () {
		float angle=(mPhase+Time.time*mSpeed)/180*Mathf.PI;
		Vector3 pos=new Vector3(Mathf.Cos(angle)*mA.x+Mathf.Sin(angle)*mB.x,Mathf.Cos(angle)*mA.y+Mathf.Sin(angle)*mB.y,Mathf.Cos(angle)*mA.z+Mathf.Sin(angle)*mB.z);
		transform.position=pos*mRotationRadius+transform.parent.position;
	}
}
