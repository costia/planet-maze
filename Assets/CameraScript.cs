﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour {
	
	public float  mDistance;
	public float mSpeed;
	Vector3 mRotation;
	// Use this for initialization
	void Start () {
		transform.localPosition=new Vector3(0,mDistance,mDistance);
		transform.LookAt(transform.parent);
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButton("Look")){
			mRotation+=new Vector3(mSpeed * Input.GetAxis ("Mouse Y"),mSpeed * Input.GetAxis ("Mouse X"),0);
			transform.parent.localRotation=Quaternion.Euler(mRotation);
			//transform.parent.Rotate( mSpeed * Input.GetAxis ("Mouse Y"),mSpeed * Input.GetAxis ("Mouse X"),0,Space.World);
		}
		if (Input.GetAxis("Mouse ScrollWheel") > 0){
			mDistance*=0.9f;
			transform.localPosition=transform.localPosition.normalized*mDistance;
		}
		if (Input.GetAxis("Mouse ScrollWheel") < 0){
			mDistance*=1.1f;
			transform.localPosition=transform.localPosition.normalized*mDistance;
		}
	}
}
