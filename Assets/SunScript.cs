﻿using UnityEngine;
using System.Collections;

public class SunScript : MonoBehaviour {
	float mGrowthRate=0.05f;
	public PlayerScript mPlayer;
	public GUIText mText;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		transform.localScale=transform.localScale+Vector3.one*mGrowthRate;
		float dist= (transform.position-mPlayer.transform.position).magnitude;
		if (dist<transform.localScale.x/2){
			mText.text="LOST";
			mText.color=Color.red;
		}
	}
}
