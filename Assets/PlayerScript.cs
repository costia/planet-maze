﻿using UnityEngine;
using System.Collections;

public class PlayerScript : MonoBehaviour {
	public GameObject mCurrentPlanet;
	GameObject mTarget;
	float maxJumpDist=15;
	GameObject[] mPlanets;
	enum PlayerState {OnPlanet};
	PlayerState mState;
	// Use this for initialization
	void Start () {
		mPlanets=GameObject.FindGameObjectsWithTag("Planet");
		mCurrentPlanet.GetComponent<Renderer>().material.color=Color.red;

	}
	
	// Update is called once per frame
	void Update () {
		switch (mState){
		case PlayerState.OnPlanet:
			this.transform.parent=mCurrentPlanet.transform;
			this.transform.position=mCurrentPlanet.transform.position;
			break;
		}
		
		float minDist=maxJumpDist*2;
		int minInd=0;
		for (int i=0;i<mPlanets.Length;i++){
			if (mPlanets[i]==mCurrentPlanet){
				continue;
			}
			Vector3 diff=mCurrentPlanet.transform.position-mPlanets[i].transform.position;
			float dist=diff.magnitude;
			if (dist<minDist){
				minDist=dist;
				minInd=i;
			}
		}
		if (minDist<maxJumpDist){
			mTarget=mPlanets[minInd];
			mTarget.GetComponent<PlanetScript>().Select();
		}else if (mTarget!=null) {
			mTarget.GetComponent<PlanetScript>().DeSelect();
			mTarget=null;			
		}
		
		if (((Input.GetButtonDown("Jump"))&&(mTarget!=null)) ||
		    ((Input.touchCount>0)&&(Input.GetTouch(0).phase==TouchPhase.Began)) ){
			mTarget.GetComponent<PlanetScript>().DeSelect();
			mCurrentPlanet.GetComponent<Renderer>().material.color=Color.white;
			mCurrentPlanet=mTarget;
			mCurrentPlanet.GetComponent<Renderer>().material.color=Color.red;
			mTarget=null;
			
		}

	
	}
}
